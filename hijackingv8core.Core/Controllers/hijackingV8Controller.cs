﻿﻿using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace hijackingv8core.Core.Controllers
{
    public class hijackingV8Controller : RenderMvcController
    {
        public ActionResult Index(ContentModel model)
        {
            Logger.Info<hijackingV8Controller>($"{model.Content.Id} value of the webhook is {Request.Params["webhook(s)"]}");
            return CurrentTemplate(model);

        }

    }
}
